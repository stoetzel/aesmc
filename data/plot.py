import numpy as np
import matplotlib.pyplot as plt

a = np.array([])
b = np.array([])
c = np.array([])
z = np.array([])
sim = np.array([])
var = np.array([])
sdp = np.array([])
sdm = np.array([])

filex1 = 'x0'
filext = 'xf'
filey = 'y0'
filez = 'z'
filesim = 'sim'
filevar = 'var'

f = open(filex1, 'r')
for i in f:
    a = np.append(a,[float(i)])
f.close()
f = open(filext, 'r')
for i in f:
    b = np.append(b,[float(i)])
f.close()
f = open(filey, 'r')
for i in f:
    c = np.append(c,[float(i)])
f.close()
f = open(filez, 'r')
for i in f:
    z = np.append(z,[float(i)])
f.close()
f = open(filesim, 'r')
for i in f:
    sim = np.append(sim,[float(i)])
f.close()
f = open(filevar, 'r')
for i in f:
    var = np.append(var,[float(i)])
f.close()

sdp = sim+var
sdm = sim-var

plt.hist(a, bins=50, rwidth=0.9,color='blue', label='$x_1$')
plt.hist(b, bins=50, rwidth=0.9,color='green', label='$x_T$')
plt.hist(c, bins=50, rwidth=0.9,color='red', label='$y$')
#plot labels
plt.xlabel('x')
plt.ylabel('#ofpoints')
plt.grid(True)
plt.legend()

plt.show()
plt.plot(sim, 'red', label='smc-mean')
plt.plot(sdp, 'red', linestyle='dashed', label='sd')
plt.plot(sdm, 'red', linestyle='dashed')
plt.xlabel('timesteps $t$')
plt.ylabel('$y_t$')
plt.grid(True)
plt.plot(c, label='data')
plt.legend()
plt.show()

#plt.plot(z)
#plt.show()
