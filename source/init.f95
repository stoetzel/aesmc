module init

  implicit none

contains
  !check for real randomness
  real function random()
    call random_number(random)
  end function
end module
