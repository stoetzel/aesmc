module output

  use ieee_arithmetic

  implicit none

contains

  !> \brief           Writes two given arrays into a file
  !! \details         Writing format is two colums, real numbers
  !! \author          Tim Stötzel
  !! \date            16.01.2019
  !! \param[in]       unitnumber        Unit number of the file
  !! \param[in]       x_values          Array of values to be plotted on the X-Axis
  !! \param[in]       y_values          Array of values to be plotted on the Y-Axis
  subroutine writefile(unitnumber, f_values, io_error)
    integer, intent(in)               :: unitnumber
    real, dimension(:,:), intent(in)    :: f_values
    integer, intent(in)               :: io_error
      !do n=1, size(f_values,1)
        if (io_error == 0) then
          write(unitnumber,rec=1) f_values !(n,:)
        else
          print*, "error occured during write, iostat value: ", io_error
        end if
      !end do
  end subroutine

  subroutine writeData(unit, io_error, params, translation, time, points, int, var, finalopt, finalquality)
    real, dimension(:,:), intent(in)                    :: params
    real, dimension(:), intent(in)                      :: translation
    real, intent(in)                                    :: time, int, var, finalopt, finalquality
    integer, intent(in)                                 :: points, io_error, unit
    integer                                             :: m,n
    if(io_error == 0) then
      write(unit,*) "Parameters: "
      write(unit,*) params(1,1)
      write(unit,*) params(1,2)
      write(unit,*) params(2,1)
      write(unit,*) params(2,2)
      write(unit,*) translation(1)
      write(unit,*) translation(2)
      write(unit,*) "integral"
      write(unit,*) int
      write(unit,*) "variance"
      write(unit,*) var
      write(unit,*) "final optimizer value"
      write(unit,*) finalopt
      write(unit,*) "final q-factor"
      write(unit,*) finalquality
      write(unit,*) "NumberofPoints"
      write(unit,*) points**2
      write(unit,*) "computation_time"
      write(unit,*) time
    else
      print*, "error occured during writeData, iostat value: ", io_error
    end if

  end subroutine

  subroutine writeArray(unitnumber, array, io_error)
    integer, intent(in)               :: unitnumber
    real, dimension(:), intent(in)    :: array
    integer, intent(in)               :: io_error

    if(io_error == 0) then
      write(unitnumber, rec=1) array
    else
      print*, "error occured during write, iostat value: ", io_error
    end if
  end subroutine

  subroutine writeArrayCLEAN(unitnumber, array, io_error)
    integer, intent(in)               :: unitnumber
    real, dimension(:), intent(in)    :: array
    integer, intent(in)               :: io_error
    integer                           :: i

    if(io_error == 0) then
      do i=1, size(array)
        write(unitnumber, *) array(i)
      end do
    else
      print*, "error occured during write, iostat value: ", io_error
    end if
  end subroutine

  subroutine progress(current, max, i)
    real, intent(in)            :: current, max
    integer, intent(in)         :: i

    if(int(floor(current/max*100.0))-i>0) then
      print*, int(floor(current/max*100.0))
    end if

  end subroutine

  subroutine progInt(current, max)
    integer, intent(in)                             :: current, max

      if(max>=100) then
        if(mod(current,max/100)==0) then
          print*, int(real(current)/real(max)*100)
        end if
      end if
  end subroutine

  subroutine printParams(params, translation)
    real, dimension(:,:), intent(in)      :: params
    real, dimension(:), intent(in)        :: translation
    integer                               :: i,j

    print*, "Transformation matrix: "
    do i=1, size(params(:,1))
      do j=1, size(params(1,:))
        print*, params(i,j)
      end do
    end do
    print*, "Translation: "
    do i=1, size(translation)
      print*, translation(i)
    end do
  end subroutine

  subroutine printarray(array)
    real, dimension(:,:), intent(in)        :: array
    integer                                 :: i,k

    do i=1, size(array(1,:))
      print*, array(i,:)
    end do
  end subroutine

  subroutine checkNumber(nr_int, nr_real, mode, msg)
    real, intent(in)                                :: nr_real
    integer, intent(in)                             :: nr_int
    character(*), intent(in)                        :: mode, msg

    if(mode == "i") then
      if(.NOT. ieee_is_normal(real(nr_int))) then
        print*, "numberical error in ", msg, nr_int
        stop
      end if
    else if(mode == "r") then
      if(.NOT. ieee_is_normal(nr_real)) then
        print*, "numerical error in ", msg, nr_real
        stop
      end if
    end if
  end subroutine

  subroutine breakpoint()
    print*, "breakpoint reached!"
    stop
  end subroutine

end module output
