program main

  use tools
  use output

  implicit none
  !===============variable declaration
  real, allocatable, dimension(:,:)                 :: x, w, wn, a
  real, allocatable, dimension(:)                   :: z, obs, filtermean, simulated, var
  real, dimension(2)                                :: theta
  real                                              :: mu, sigsq, alpha, beta, v1, v2
  integer                                           :: tmax, kmax, i, io_error, unit,j

  !set global variables
  tmax = 500
  kmax = 1000
  theta = (/1.,1./)

  !State space model parameters
  mu = 0.
  sigsq = 1.
  alpha = 1.0
  beta = 1.0
  v1 = 1.0
  v2 = 0.1

  !output variables
  unit = 15

  !initialize arrays
  allocate(obs(tmax))
  allocate(var(tmax))
  allocate(filtermean(tmax))
  allocate(simulated(kmax))
  allocate(x(tmax,kmax))
  allocate(w(tmax,kmax))
  allocate(wn(tmax,kmax))
  allocate(a(tmax,kmax))
  allocate(z(tmax))

  !for debugging: call breakpoint()

  !===============INIT LGSSM==========================
  !using paper parameters
  print*, "Simulating data"
  theta = (/0.9,1.0/)

  x(1,1) = normal(mu, sigsq)

  do i=2, tmax
    x(i,1) = theta(1)*x(i-1,1)+normal(0.,1.)
  end do

  do i=1, tmax
    obs(i) = theta(2)*x(i,1)+sqrt(0.1)*normal(0.,1.)
  end do

  !=============INIT SMC STARTING VALUES==============
  !init initial x and observables
  print*, "Initialize particles"
  theta=(/0.9,1.0/)
  do i=1, kmax
    x(1,i) = normal(0.0,1.0)
  end do

  open(unit = unit, file='data/x0', iostat = io_error, status = 'replace')
  call  writeArrayCLEAN(unit, x(1,:), io_error)
  close(unit = unit, status='keep')

  !calculate weights
  do i=1, kmax
    w(1,i) = weight(x(1,:), obs(1), i, theta)
  end do

  !norm weights
  do i=1, kmax
    wn(1,i) = normweights(w(1,:),i)
  end do

  z(1) = zsmc(w,1)

  !===============SMC=================================
  print*, "Processing"
  do i=2, tmax
    call progInt(i, tmax)
    call smc(x, obs, a, w, wn, i, theta,z)
    !filtermean(i-1) = avg(x(i-1,:))
  end do

  !filtermean(tmax) = avg(x(tmax-1,:))

  do i=1, tmax
    do j=1, kmax
      simulated(j) = normal(theta(2)*x(i,j), 0.1)
    end do
    filtermean(i) = avg(simulated)
    var(i) = variance(simulated)
    !simulated(i) = normal(theta(2)*filtermean(i), 0.1)!y_prop(filtermean, simulated, theta, i)!/z(i)
  end do

  !===============DATA OUTPUT=========================
  print*, "ML estimator: ", z

  open(unit = unit, file='data/y0', iostat = io_error, status = 'replace')
  call writeArrayCLEAN(unit, obs, io_error)
  close(unit = unit, status='keep')

  open(unit = unit, file='data/xf', iostat = io_error, status = 'replace')
  call writeArrayCLEAN(unit, x(tmax,:), io_error)
  close(unit = unit, status='keep')

  open(unit = unit, file='data/z', iostat = io_error, status = 'replace')
  call writeArrayCLEAN(unit, z, io_error)
  close(unit = unit, status='keep')

  open(unit = unit, file='data/sim', iostat = io_error, status = 'replace')
  call writeArrayCLEAN(unit, filtermean, io_error)
  close(unit = unit, status='keep')

  open(unit = unit, file='data/var', iostat = io_error, status = 'replace')
  call writeArrayCLEAN(unit, var, io_error)
  close(unit = unit, status='keep')

  contains

end program
