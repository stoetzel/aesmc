module tools

  use init
  use ieee_arithmetic

  implicit none
  real(8), parameter                                   :: pi=4.D0*DATAN(1.D0)

  contains
    !online-variance, welfords algorithm
    !returns variance of rndVar
    real function variance(elements)
      real, dimension(:), intent(in)                :: elements
      real                                          :: mean, oldmean
      integer                                       :: i

      variance = 0.
      mean = 0.
      do i=1, size(elements)
        oldmean = mean
        mean = mean + (elements(i)-mean)/real(i)
        variance = variance + (elements(i)-mean)*(elements(i)-oldmean)
      end do
      variance = variance/(real(size(elements)-1))
    end function

    !online-average calculation
    !Return:  - real, average of 1d array
    !Input:   - real, array(), 1d array
    real function avg(elements)
      real, dimension(:), intent(in)                :: elements
      integer                                       :: i, max

      max = size(elements)
      avg = 0.

      do i=1, max
        avg = avg + (elements(i)-avg)/real(max)
      end do
    end function

    !Box-Mueller algorithm to sample Normal(0,1) distribution
    !Return:  - normal(mu, var) distributed element
    !Input:   - real mu, mean value
    !         - real var, variance(sigma squared)
    real function normal(mu, var)
      real, intent(in)                              :: mu, var
      real                                          :: r1, r2

      r1 = random()
      r2 = random()
      !generate N(0,1) distributed rv
      normal = sqrt(-2*log(r1))*cos(2*pi*r2)
      !generate N(mu,sig) distributed rv from the N(0,1) distribuited one
      normal = mu + sqrt(var)*normal

    end function

    !weight calculator
    !Return:  - real, weight of particle x_index at time t
    !Input:   - real array(), xt, yt, particles at t and observables at t
    !         - real array(), theta learning paramter
    !         - integer index, current particle index
    real function weight(xt, yt, index, theta)
      real, dimension(:), intent(in)                :: xt, theta
      real, intent(in)                              :: yt
      integer, intent(in)                           :: index

      weight = 1./sqrt(2.*pi*0.1)*exp(-(yt-theta(2)*xt(index))**2/(2.*0.1))

    end function

    !norm weights at time t
    !Return:  - real, array(), normed particle weights
    !Input:   - real, array(), weights at time t
    !         - integer, particle index
    real function normweights(wt, index)
      real, dimension(:), intent(in)                :: wt
      integer, intent(in)                           :: index
      normweights = wt(index)/sum(wt)
    end function

    !marginal likelihood estimator
    !Return   - real, zsmc estimator
    !Input    - real, array(,), weights
    !         - integer, current time step
    real function zsmc(w, current)
      real, dimension(:,:), intent(in)              :: w
      integer, intent(in)                           :: current
      integer                                       :: i

      !REPLACE WITH ONLINE AVERAGE
      zsmc = 1.
      do i=1, current
        zsmc = zsmc + log(avg(w(i,:)))
      end do
      !zsmc = log(avg(w(current,:)))-(i-1)*log(real(size(w(1,:))))
    end function

    !samples distribution for particles
    !Return   - real, x sampled from distribution
    !Input    - real, array(), particles from timestep t-1
    !         - integer, k, particle index
    real function xdist(xold, theta,k)
      real, dimension(:), intent(in)                :: xold, theta
      integer, intent(in)                           :: k
      xdist = normal(theta(1)*xold(k), 1.)
    end function

    !resampling
    subroutine resample(xt, at, wnt)
      real, dimension(:), intent(inout)             :: at
      real, dimension(:), intent(in)                :: xt, wnt
      real, allocatable, dimension(:)               :: u
      integer, allocatable, dimension(:)            :: n
      real                                          :: sum1, sum2
      integer                                       :: i, j, m

      m = size(xt)

      allocate(u(m))
      allocate(n(m))

      n = 0

      u(1) = random()*1/real(m)
      do i=2, m
        u(i) = u(1) + (i-1)/real(m)
      end do

      do i=1, m
        if(i==1) then
          sum1 = 0.
          sum2 = sum1 + wnt(1)
        else
          sum1 = sum(wnt(1:i-1))
          sum2 = sum1 + wnt(i)
        end if
        do j=1, m
          if(u(j)>=sum1 .AND. u(j)<sum2) then
            n(i) = n(i) + 1
          end if
        end do
      end do

      !sample from the new distribution given by P(x_i)=n(i)/m
      do j=1, m
        if(n(j)/=0) then
          if(j==1) then
            do i=1, n(j)
              at(i) = xt(j)
            end do
          else
            do i=int(sum(n(1:j-1)))+1, int(sum(n(1:j)))
              at(i) = xt(j)
            end do
          end if
        end if
      end do


    end subroutine

    !subroutine for smc algorithm
    !Input    - real, array(,) x,y,resampling,w,wn
    !         - integer, t, current time step
    subroutine smc(x,y,a,w,wn,t,theta,z)
      real, dimension(:,:), intent(inout)           :: a, x, w, wn
      real, dimension(:), intent(in)                :: y
      real, dimension(:), intent(inout)             :: z
      real, dimension(:), intent(in)                :: theta
      integer, intent(in)                           :: t
      integer                                       :: i

      !if(int(1./sum(wn(t-1,:)**2))<size(x(1,:))/2.) then
        !resample particles
        call resample(x(t-1,:), a(t-1,:), wn(t-1,:))
        x(t-1,:) = a(t-1,:)
      !end if

      !sample generation for timestep t
      do i=1, size(x(t,:))
        x(t,i) = xdist(x(t-1,:), theta, i)
      end do

      !calculate weights
      do i=1, size(x(t,:))
        w(t,i) = weight(x(t,:), y(t), i, theta)
      end do

      !norm weights
      do i=1, size(x(t,:))
        wn(t,i) = normweights(w(t,:),i)
      end do

      z(t) = zsmc(w, t)

      if(.NOT. ieee_is_normal(z(t))) then
        print*, t
        print*, sum(w(t,:))
        print*, "something is not right with z"
        print*, z
        stop
      end if
    end subroutine


end module
